#!/bin/bash

if [[ -z "$1" ]]; then
    echo -e "Usage: bash runLighthouse.sh [URL]"
    echo -e "Example: bash runLighthouse.sh https://example.com\n"
else
    CHROME_PATH=/usr/bin/chromium lighthouse $1 --no-enable-error-reporting --chrome-flags="--headless --no-sandbox" --output=json
fi