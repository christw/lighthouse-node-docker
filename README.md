# lighthouse-node-docker
A docker container which is running nodejs webserver (using `express`), runs lighthouse cli ([lighthouse](https://www.npmjs.com/package/lighthouse) npm package)
and returns lighthouse json response as response to a web request.

NOTE: prototype only, use at your own risk.

## usage

```sh
docker-compose up -d    # starting docker container
curl 127.0.0.1:3001/?url=https%3A%2F%2Fexample.com  # requesting lighthouse report for https://example.com
```
