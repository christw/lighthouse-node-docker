const { exec } = require('child_process');
const express = require('express');
const app = express();
const port = 3000;

const urlPattern = /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([/\w .-]*)*\/?$/;

app.get('/', (req, res) => {
  // Validate 'urlParam' against regex pattern
  if (!urlPattern.test(req.query.url)) {
    return res.status(400).json({ error: 'Invalid URL format' });
  }

  const runLighthouseCmd = `runLighthouse.sh ${req.query.url}`;

  // Use the exec function to run the Bash script
  exec(`bash ${runLighthouseCmd}`, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error executing script: ${stderr}`);
      return res.status(500).send('Internal Server Error');
    }

    console.log(`Script output: ${stdout}`);
    res.send(stdout);
  });
})

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})

