FROM node:20-slim
WORKDIR /app
RUN apt-get update && apt-get install -y chromium
COPY ./yarn.lock .
COPY ./package.json .
RUN yarn install
COPY ./runLighthouse.sh .
COPY ./node webserver
RUN ln -s /app/node_modules/lighthouse/cli/index.js /usr/bin/lighthouse
CMD ["nodejs", "webserver/index.js"]